from ntpath import join
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class LabelWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title="Label Example")

        grid = Gtk.Grid()

        self.entry = Gtk.Entry()
        self.entry.set_text("")
        
        Frame = Gtk.Frame()
        Frame.set_label("ข้อมูลสำหรับคำนวนเลข")
        Frame.add(self.entry)
        grid.attach(Frame, 0, 0, 1, 1)
        
        self.buttonde = Gtk.Button(label="delete")
        self.buttonde.connect("clicked", self.on_delete_clicked)
        grid.attach(self.buttonde, 0, 1, 1, 1)
        
        self.buttonca = Gtk.Button(label="Calculate")
        self.buttonca.connect("clicked", self.on_calculate_clicked)
        grid.attach_next_to(self.buttonca, self.buttonde, Gtk.PositionType.RIGHT, 2, 1)
        
        label0 = Gtk.Label(label="0")
        grid.attach_next_to(label0, self.buttonde, Gtk.PositionType.BOTTOM, 1, 2)
        
        
        label1 = Gtk.Label(label="1")
        grid.attach_next_to(label1, label0, Gtk.PositionType.RIGHT, 1, 2)
        label2 = Gtk.Label(label="2")
        grid.attach_next_to(label2, label1, Gtk.PositionType.RIGHT, 1, 2)
        label3 = Gtk.Label(label="3")
        grid.attach_next_to(label3, label2, Gtk.PositionType.RIGHT, 1, 2)
        label4 = Gtk.Label(label="4")
        grid.attach_next_to(label4, label3, Gtk.PositionType.RIGHT, 1, 2)
        label5 = Gtk.Label(label="5")
        grid.attach_next_to(label5, label4, Gtk.PositionType.RIGHT, 1, 2)
        label6 = Gtk.Label(label="6")
        grid.attach_next_to(label6, label5, Gtk.PositionType.RIGHT, 1, 2)
        label7 = Gtk.Label(label="7")
        grid.attach_next_to(label7, label6, Gtk.PositionType.RIGHT, 1, 2)
        label8 = Gtk.Label(label="8")
        grid.attach_next_to(label8, label7, Gtk.PositionType.RIGHT, 1, 2)
        label9 = Gtk.Label(label="9")
        grid.attach_next_to(label9, label8, Gtk.PositionType.RIGHT, 1, 2)
        
        
        labelMost = Gtk.Label(label="HighLight :")
        grid.attach_next_to(labelMost, Frame, Gtk.PositionType.BOTTOM, 1, 2)
        self.entryMost = Gtk.Entry()
        self.entryMost.set_text("")
        grid.attach_next_to(self.entryMost, labelMost, Gtk.PositionType.RIGHT, 1, 1)
        
        self.entry0 = Gtk.Entry()
        self.entry0.set_text("0")
        self.entry0.set_width_chars(2)
        grid.attach_next_to(self.entry0, label0, Gtk.PositionType.BOTTOM, 1, 2)
        self.entry1 = Gtk.Entry()
        self.entry1.set_text("0")
        self.entry1.set_width_chars(2)
        grid.attach_next_to(self.entry1, label1, Gtk.PositionType.BOTTOM, 1, 2)
        self.entry2 = Gtk.Entry()
        self.entry2.set_text("0")
        self.entry2.set_width_chars(2)
        grid.attach_next_to(self.entry2, label2, Gtk.PositionType.BOTTOM, 1, 2)
        self.entry3 = Gtk.Entry()
        self.entry3.set_text("0")
        self.entry3.set_width_chars(2)
        grid.attach_next_to(self.entry3, label3, Gtk.PositionType.BOTTOM, 1, 2)
        self.entry4 = Gtk.Entry()
        self.entry4.set_text("0")
        self.entry4.set_width_chars(2)
        grid.attach_next_to(self.entry4, label4, Gtk.PositionType.BOTTOM, 1, 2)
        self.entry5 = Gtk.Entry()
        self.entry5.set_text("0")
        self.entry5.set_width_chars(2)
        grid.attach_next_to(self.entry5, label5, Gtk.PositionType.BOTTOM, 1, 2)
        self.entry6 = Gtk.Entry()
        self.entry6.set_text("0")
        self.entry6.set_width_chars(2)
        grid.attach_next_to(self.entry6, label6, Gtk.PositionType.BOTTOM, 1, 2)
        self.entry7 = Gtk.Entry()
        self.entry7.set_text("0")
        self.entry7.set_width_chars(2)
        grid.attach_next_to(self.entry7, label7, Gtk.PositionType.BOTTOM, 1, 2)
        self.entry8 = Gtk.Entry()
        self.entry8.set_text("0")
        self.entry8.set_width_chars(2)
        grid.attach_next_to(self.entry8, label8, Gtk.PositionType.BOTTOM, 1, 2)
        self.entry9 = Gtk.Entry()
        self.entry9.set_text("0")
        self.entry9.set_width_chars(2)
        grid.attach_next_to(self.entry9, label9, Gtk.PositionType.BOTTOM, 1, 2)
    
    
        self.add(grid)
        
    def on_delete_clicked(self, widget):
        self.entry.set_text("") 
        self.entry0.set_text("0") 
        self.entry1.set_text("0") 
        self.entry2.set_text("0") 
        self.entry3.set_text("0") 
        self.entry4.set_text("0") 
        self.entry5.set_text("0") 
        self.entry6.set_text("0") 
        self.entry7.set_text("0") 
        self.entry8.set_text("0") 
        self.entry9.set_text("0") 
        self.entryMost.set_text("") 
    def on_calculate_clicked(self, widget):
        list = [0,0,0,0,0,0,0,0,0,0]
        for x in self.entry.get_text():
            if(ord(x)>=48 and ord(x)<=57):
                list[int(x)]+=1
        self.entry0.set_text(f'{list[0]}') 
        self.entry1.set_text(f'{list[1]}') 
        self.entry2.set_text(f'{list[2]}')
        self.entry3.set_text(f'{list[3]}')
        self.entry4.set_text(f'{list[4]}')
        self.entry5.set_text(f'{list[5]}')
        self.entry6.set_text(f'{list[6]}')
        self.entry7.set_text(f'{list[7]}')
        self.entry8.set_text(f'{list[8]}')
        self.entry9.set_text(f'{list[9]}')
        
        list2 = [0,0,0,0,0,0,0,0,0,0]
        listOUT = []
        for i in range(0,10):
            max = -1
            maxPos = -1
            j = 0
            for x in list:
                if(x>max and list2[j]==0):
                    max = x
                    maxPos = j
                j+=1
            listOUT.append(maxPos)
            list2[maxPos]=1
        strings =' '
        for x in listOUT:
            strings +=str(x)
        self.entryMost.set_text(strings)


window = LabelWindow()
window.connect("destroy", Gtk.main_quit)
window.show_all()
Gtk.main()